require 'test_helper'

class OrderTest < Minitest::Test
  def test_data
    TestData::Product::Book.then do |book|
      {
        customer: TestData::Customer::Build,
        address: TestData::Address::Build,
        books: [book, book],
        book: book
      }
    end
  end

  def test_initialization_constraints
    assert_raises ArgumentError do
      Order.new
    end
  end

  def test_add_product
    order = Order.new(test_data[:customer].call)

    assert_raises ArgumentError do
      order.add_product
    end

    assert_raises ArgumentError, /Invalid product:/ do
      order.add_product Product.new
    end
  end

  def test_items
    customer, book = test_data.slice(:customer, :book).values.map(&:call)

    order = Order.new(customer)

    order.add_product book

    order_item = order.items.first

    assert_equal order, order_item.order
    assert_equal book, order_item.product

    assert order.items.all?(OrderItem)
  end

  def test_total_amount
    books = test_data[:books].map(&:call)
    order = Order.new(test_data[:customer])

    books.each(&order.method(:add_product))

    assert_equal 20, order.total_amount
  end

  def test_close
    paid_at = Time.now

    payment = Minitest::Mock.new
    payment.expect(:paid?, true)
    payment.expect(:paid_at, paid_at)

    order = Order.new(test_data[:customer])

    assert_nil order.closed_at

    order.close(payment)

    assert_equal paid_at, order.closed_at

    assert_mock payment
  end

  def test_is_paid
    payment = Minitest::Mock.new
    payment.expect(:paid_at, Time.now)
    2.times { payment.expect(:paid?, true) }

    order = Order.new(test_data[:customer])
                 .tap { |ord| ord.close(payment) }

    assert order.paid?

    assert_mock payment
  end

  def test_address
    customer = test_data[:customer].call

    order = Order.new(customer)

    assert_instance_of Address, order.address
    refute_empty order.address.zipcode

    address = test_data[:address].call

    assert_equal address, Order.new(customer, address: address).address
  end

  def test_customer
    customer = test_data[:customer]

    order = Order.new(customer)

    assert_equal customer, order.customer
  end
end
