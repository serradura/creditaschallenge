require 'test_helper'

class AddressTest < Minitest::Test
  def test_initialization_constraints
    assert_raises ArgumentError do
      Address.new
    end
  end

  def test_zipcode
    address = Address.new zipcode: '03178080'

    assert_equal '03178080', address.zipcode
  end

  def test_is_valid
    assert Address.new(zipcode: '03178080').valid?

    refute Address.new(zipcode: nil).valid?
    refute Address.new(zipcode: '').valid?
  end
end
