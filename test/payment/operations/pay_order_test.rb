require 'test_helper'

class Payment::Operations::PayOrderTest < Minitest::Test
  def test_data
    {
      credit_card: TestData::PaymentMethod::CreditCard,
      invoice: TestData::PaymentInvoice::Build,
      order: TestData::Order::WithProduct
    }
  end

  def test_payment_and_order_state_after_pay_with_success
    order = test_data[:order].call
    credit_card = test_data[:credit_card].call

    payment_result =
      Payment::Operations::PayOrder
        .new(order, payment_method: credit_card)
        .call

    assert payment_result.success?

    payment = payment_result.value

    assert payment.paid?
    assert_kind_of Time, payment.paid_at

    assert_kind_of Numeric, payment.authorization_number

    assert_equal order.items.first.total, payment.amount

    payment_invoice = payment.invoice

    assert_instance_of Payment::Invoice, payment_invoice
    assert_equal order.address, payment_invoice.billing_address
    assert_equal order.address, payment_invoice.shipping_address

    assert_equal credit_card, payment.payment_method

    assert payment.order.closed?
    assert_kind_of Time, payment.order.closed_at
  end

  def test_paid_at_when_it_was_received_with_the_pay_event
    paid_at = Time.now
    order = test_data[:order].call
    credit_card = test_data[:credit_card].call

    payment_result =
      Payment::Operations::PayOrder
        .new(order, payment_method: credit_card)
        .call(paid_at: paid_at)

    assert_equal paid_at, payment_result.value.paid_at
  end
end
