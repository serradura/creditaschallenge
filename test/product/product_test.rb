require 'test_helper'

class ProductTest < Minitest::Test
  def test_initialization_constraints
    assert_raises ArgumentError do
      Product.new name: 'name'
    end

    assert_raises ArgumentError do
      Product.new type: Product::Types::BOOK
    end
  end

  def test_name
    product = Product.new name: 'name', type: Product::Types::BOOK

    assert_equal 'name', product.name
  end

  def test_type
    type = Product::Types::BOOK

    product = Product.new name: nil, type: type

    assert_equal type, product.type
  end

  def test_is_valid
    type = Product::Types::BOOK

    assert Product.new(name: 'name', type: type).valid?

    refute Product.new(name: nil, type: type).valid?
  end

  def test_is_book
    assert Product.as_book(name: 'foo').book?

    refute Product.as_digital(name: 'foo').book?
  end

  def test_is_digital
    assert Product.as_digital(name: 'foo').digital?

    refute Product.as_book(name: 'foo').digital?
  end

  def test_is_physical
    assert Product.as_physical(name: 'foo').physical?

    refute Product.as_book(name: 'foo').physical?
  end

  def test_is_membership
    assert Product.as_membership(name: 'foo').membership?

    refute Product.as_book(name: 'foo').membership?
  end
end
