require 'test_helper'

class CustomerTest < Minitest::Test
  def test_name
    assert_equal Customer::DEFAULT_NAME, Customer.new.name

    assert_equal 'Foolano', Customer.new('Foolano').name
  end

  def test_has_membership
    membership = Minitest::Mock.new
    membership.expect :activated?, true
    membership.expect :nil?, false

    customer = Customer.new

    refute customer.has_membership?

    customer.activate_membership(membership)

    assert customer.has_membership?

    assert_mock membership
  end

  def test_add_a_discount_for_next_payment
    customer = Customer.new

    assert_equal 0, customer.total_discount

    2.times { customer.add_a_discount_for_next_payment(10) }

    assert_equal 20, customer.total_discount
  end
end
