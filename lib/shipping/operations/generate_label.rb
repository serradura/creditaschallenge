module Shipping
  module Operations
    class GenerateLabel
      extend Resultable::Mixin

      ALLOWED_PRODUCTS = [
        Product::Types::BOOK,
        Product::Types::PHYSICAL
      ].freeze

      def self.call(order_item)
        return Failure(:unpaid_order) unless order_item.paid_order?

        product = order_item.product

        if ALLOWED_PRODUCTS.include?(product.type)
          Success(Label.new(product).tap(&:save))
        else
          Failure(:invalid_product)
        end
      end
    end
  end
end
