class Customer
  module Operations
    class GrantDiscountForDigitalProduct
      extend Resultable::Mixin

      def self.call(order_item)
        return Failure(:unpaid_order) unless order_item.paid_order?
        return Failure(:invalid_product) unless order_item.product.digital?

        customer = order_item.customer
        customer.add_a_discount_for_next_payment(10)

        Success(customer)
      end
    end
  end
end
