class Customer
  module Operations
    class ActivateMembership
      extend Resultable::Mixin

      def self.call(order_item)
        return Failure(:unpaid_order) unless order_item.paid_order?
        return Failure(:invalid_product) unless order_item.product.membership?

        membership = Membership.new(order_item, activated: true)

        customer = order_item.customer.activate_membership(membership)

        Success(customer)
      end
    end
  end
end
