class Customer
  require_relative 'operations/activate_membership'
  require_relative 'operations/grant_discount_for_digital_product'

  attr_reader :name, :membership

  DEFAULT_NAME = 'John Doe'

  def initialize(name = DEFAULT_NAME)
    @name = name
    @discounts = []
    @membership = nil
  end

  def activate_membership(membership)
    @membership = membership if membership.activated?
  end

  def has_membership?
    !@membership.nil?
  end

  def add_a_discount_for_next_payment(value)
    return false unless value.is_a?(Numeric) && value > 0

    @discounts << value

    true
  end

  def total_discount
    @discounts.reduce(0, &:+)
  end
end
