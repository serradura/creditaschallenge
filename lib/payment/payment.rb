class Payment
  require_relative 'invoice/invoice'
  require_relative 'methods/credit_card'
  require_relative 'operations/pay_order'

  attr_reader :authorization_number, :amount, :invoice, :order, :payment_method, :paid_at

  def initialize(attributes = {})
    @authorization_number, @amount = attributes.values_at(:authorization_number, :amount)
    @invoice, @order = attributes.values_at(:invoice, :order)
    @payment_method = attributes.fetch(:payment_method)
  end

  def pay(paid_at)
    return false unless can_be_paid? || !paid_at.is_a?(Time)

    @paid_at = paid_at

    true
  end

  def paid?
    !paid_at.nil?
  end

  private

  def can_be_paid?
    !paid? &&
    (amount && amount > 0) &&
    authorization_number.is_a?(::Numeric) &&
    (invoice.is_a?(Invoice) && invoice.valid?) &&
    (order.is_a?(::Order) && order.valid?)
  end
end
