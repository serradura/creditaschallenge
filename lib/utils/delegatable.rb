require 'forwardable'

module Delegatable
  def self.included(klass)
    klass.extend(Forwardable)
    klass.extend(Macros)
  end

  module Macros
    def delegate(*methods, to:, as: nil)
      if as
        def_delegator(to, methods.first, as)
      else
        def_delegators(to, *methods)
      end
    end
  end
end
