class Order
  module Operations
    class SubmitIfPaid
      include ::Resultable::Mixin

      OPERATIONS = [
        Shipping::Operations::GenerateLabel,
        Customer::Operations::ActivateMembership,
        Customer::Operations::GrantDiscountForDigitalProduct,
        Notification::Operations::SendEmail
      ].freeze

      SubmitOrderItem = -> item do
        operations = OPERATIONS
          .map { |operation| operation.call(item) }
          .select(&:success?)

        [item, operations]
      end

      def initialize(order)
        @order = order
      end

      def call
        return Failure(:unpaid_order) unless @order.paid?

        Success(@order.items.map(&SubmitOrderItem))
      end
    end
  end
end
