class Order
  require_relative 'operations/submit_if_paid'

  attr_reader :customer, :items, :payment, :address, :closed_at

  def initialize(customer, overrides = {})
    @customer = customer
    @items = []
    @order_item_class = overrides.fetch(:item_class) { OrderItem }
    @address = overrides.fetch(:address) { Address.new(zipcode: '45678-979') }
  end

  def add_product(product)
    if product.is_a?(::Product) && product.valid?
      @items << @order_item_class.new(order: self, product: product)
    else
      raise ArgumentError, "Invalid product: #{product.inspect}"
    end
  end

  def total_amount
    @items.map(&:total).inject(:+)
  end

  def close(payment)
    return false unless payment.paid?

    @payment = payment
    @closed_at = payment.paid_at

    return true
  end

  def closed?
    !closed_at.nil?
  end

  def paid?
    closed? && payment.paid?
  end

  def valid?
    (address.is_a?(::Address) && address.valid?) &&
    customer.is_a?(::Customer) &&
    items.any?
  end

  # remember: you can create new methods inside those classes to help you create a better design
end
