module Notification
  module Operations
    class SendEmail
      extend Resultable::Mixin

      BookTemplate = -> (order_item) do
        <<~EMAIL
          Pagamento realizado com sucesso!

          O livro #{order_item.product.inspect} foi separado para envio.

          Observação:
          Esse produto trata-se de um item isento de impostos conforme disposto na Constituição Art. 150, VI, d.
        EMAIL
      end

      DigitalTemplate = -> order_item do
        customer = order_item.customer

        <<~EMAIL
          #{customer.name}, sua compra realizada com sucesso."

          Acesse este <a>link</a>, para realizar o download do #{order_item.product.inspect}.

          Observação:
          Por conta deste item você possui um total de R$ #{customer.total_discount} de desconto para as próximas compras.
        EMAIL
      end

      MembershipTemplate = -> order_item do
        "#{order_item.customer.name}, sua assinatura foi ativada com sucesso."
      end

      PRODUCT_TEMPLATES = {
        Product::Types::BOOK => BookTemplate,
        Product::Types::DIGITAL => DigitalTemplate,
        Product::Types::MEMBERSHIP => MembershipTemplate
      }

      def self.call(order_item)
        return Failure(:unpaid_order) unless order_item.paid_order?

        template_builder = PRODUCT_TEMPLATES[order_item.product.type]

        return Failure(:invalid_product) unless template_builder

        Success(template_builder.call(order_item))
      end
    end
  end
end
