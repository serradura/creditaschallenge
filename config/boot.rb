raise 'Wrong Ruby version, please use a version >= 2.6' unless RUBY_VERSION >= '2.6.0'

File.expand_path('../lib', __dir__).tap do |lib_path|
  Dir[
    "#{lib_path}/utils/*.rb",
    "#{lib_path}/product/*.rb",
    "#{lib_path}/shipping/*.rb",
    "#{lib_path}/customer/*.rb",
    "#{lib_path}/notification/**/*.rb",
    "#{lib_path}/*.rb",
    "#{lib_path}/**/*.rb"
  ].then { |files| files.each(&method(:require)) }
end
