require_relative 'config/boot'


###########
# Helpers #
###########

PrintSampleTitle = -> (desc) do
  text = "#{desc} sample"

  puts
  puts '*' * text.size
  puts text
  puts '*' * text.size
  puts
end

BuildOrder = -> (customer:, product:) do
  Order.new(customer).tap do |order|
    Array(product).each { |prod| order.add_product(prod) }
  end
end

PayOrder = -> (order, with: payment_method) do
  Payment::Operations::PayOrder
    .new(order, payment_method: with)
    .call
end

PayOrderWithCreditCard = -> order do
  credit_card =
    Payment::Methods::CreditCard.fetch_by_hashed('43567890-987654367')

  PayOrder.call(order, with: credit_card)
end

PrintOrderPaymentResult = -> (result) do
  p "result.success? == #{result.success?}"

  payment = result.value

  p "payment.paid? == #{payment.paid?}"
  p "payment.order.closed? == #{payment.order.closed?}"

  result
end

SubmitOrderIfItWasPaid = -> (result) do
  return result if result.failure?

  order = result.value.order

  Order::Operations::SubmitIfPaid.new(order).call
end

TryToPrintTheOrderSubmissionResult = -> submission_result do
  return p submission_result.value if submission_result.failure?

  submission_result.value.each do |(order_item, operations)|
    puts
    print ' ' * 4
    p "product.type == '#{order_item.product.type}'"

    operations.each do |operation_result|
      print ' ' * 8
      p operation_result.value
    end
  end
end

###########
# Samples #
###########

PrintSampleTitle['Physical product']

mug = Product.as_physical(name: 'Awesome mug')

BuildOrder
  .call(customer: Customer.new('Foolano'), product: mug)
  .then(&PayOrderWithCreditCard)
  .then(&PrintOrderPaymentResult)
  .then(&SubmitOrderIfItWasPaid)
  .then(&TryToPrintTheOrderSubmissionResult)

# ---

PrintSampleTitle['Book']

book = Product.as_book(name: 'Awesome book')

BuildOrder
  .call(customer: Customer.new('Fozlano'), product: book)
  .then(&PayOrderWithCreditCard)
  .then(&PrintOrderPaymentResult)
  .then(&SubmitOrderIfItWasPaid)
  .then(&TryToPrintTheOrderSubmissionResult)


# ---

PrintSampleTitle['Membership']

subscription = Product.as_membership(name: 'Awesome subscription')

BuildOrder
  .call(customer: Customer.new('Barlano'), product: subscription)
  .then(&PayOrderWithCreditCard)
  .then(&PrintOrderPaymentResult)
  .then(&SubmitOrderIfItWasPaid)
  .then(&TryToPrintTheOrderSubmissionResult)

# ---

PrintSampleTitle['Digital product']

video = Product.as_digital(name: 'Awesome video')

BuildOrder
  .call(customer: Customer.new('Bazlano'), product: video)
  .then(&PayOrderWithCreditCard)
  .then(&PrintOrderPaymentResult)
  .then(&SubmitOrderIfItWasPaid)
  .then(&TryToPrintTheOrderSubmissionResult)


# ---

PrintSampleTitle['Random products']

products = [
  Product.as_book(name: 'Awesome book'),
  Product.as_digital(name: 'Awesome video'),
  Product.as_physical(name: 'Awesome mug'),
  Product.as_membership(name: 'Awesome subscription')
].then do |relation|
  relation[0..rand(relation.size)].shuffle
end

BuildOrder
  .call(customer: Customer.new, product: products)
  .then(&PayOrderWithCreditCard)
  .then(&PrintOrderPaymentResult)
  .then(&SubmitOrderIfItWasPaid)
  .then(&TryToPrintTheOrderSubmissionResult)
